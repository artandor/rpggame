﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1_RPG.GameObjects
{
    public class Player : Being
    {
        Random random = new Random();
        public int TotalExperience = 0;
        public enum Sexe { Male, Female };
        Sexe sexe { get; set; }
        public int tauxSucre = 0;
        public int money = 0;
        public List<Item> inventory = new List<Item>();

        public Player(string name, int currentHp, int maxHp, int currentMana, int maxMana, List<Skill> competences, int TotalExperience, Sexe sexe)
            : base(name, currentHp, maxHp, currentMana, maxMana, competences)
        {
            this.name = name;
            this.currentHp = currentHp;
            this.maxHp = maxHp;
            this.currentMana = currentMana;
            this.maxMana = maxMana;
            this.competences = competences;
            this.TotalExperience = TotalExperience;
            this.sexe = sexe;
        }

        public int GetLevel()
        {
            return this.TotalExperience / 30;
        }

        public bool Buy(Item item)
        {
            Console.WriteLine("You try to buy " + item.name + " for " + item.cost + ".");
            if(this.money - item.cost < 0)
            {
                Console.WriteLine("Woops, it looks like you don't have money for this. The item costs " + item.cost + " and you have " +
                    this.money);
                return false;
            }

            this.money -= item.cost;
            Console.WriteLine("You successfully bought " + item.name + " ! Remember that you can use it " +
                "from your inventory anytime. His effects are : " + item.effects);
            return true;
        }

        public void Loot(Creature creature)
        {
            foreach(Item item in creature.drops)
            {
                if(random.Next(1, 100) < item.lootChance)
                {
                    this.inventory.Add(item);
                    Console.WriteLine(item.name + " added to inventory. His effects are : ");
                    item.printEffects();
                }
            }

            money += creature.money;
            Console.WriteLine("You gain " + creature.money + " golds.");

            TotalExperience += creature.xpReward;
            Console.WriteLine("You gain " + creature.xpReward + " Exerience Points.");
        }

        public Skill ChooseSkill()
        {
            Skill selected = null;
            Console.WriteLine(this.name + " | Current Health : " + currentHp + " / Current Mana : " + currentMana);
            Console.WriteLine("Skills available : ");
            competences.ForEach(Console.WriteLine);
            while (selected is null)
            {
                Console.WriteLine("Choose a skill to launch :");
                string entry = Console.ReadLine();
                selected = competences.Find(competences => competences.name.Equals(entry));
                if(!(selected is null) && selected.manaCost > this.currentMana)
                {
                    Console.WriteLine("Not enough mana.");
                    selected = null;
                }
            }
            return selected;
        }

        public void Die()
        {
            Console.WriteLine("------------------------------------");
            Console.WriteLine("------------- YOU DIED -------------");
            Console.WriteLine("------------------------------------");
            Console.WriteLine();
            Console.WriteLine("Your stats at death : ");
            Console.WriteLine("Money : " + money);
            Console.WriteLine("Total Experience : " + TotalExperience);
            Console.WriteLine("Level : " + this.GetLevel());
            Console.WriteLine("Sugar level : " + this.tauxSucre);
            Console.Read();
            System.Environment.Exit(0);
        }
    }
}
