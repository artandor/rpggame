﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1_RPG.GameObjects
{
    public class Skill
    {
        public string name;
        public int damages;
        public int heals;
        public int manaCost;

        public Skill(string name, int damages, int heals, int manaCost)
        {
            this.name = name;
            this.damages = damages;
            this.heals = heals;
            this.manaCost = manaCost;
        }

        public override string ToString()
        {
            return name + "{dmg:" + damages + ", heals:"+heals+"}";
        }
    }


}
