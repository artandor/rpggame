﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1_RPG.GameObjects
{
    public class Being
    {
        public string name { get; set; }
        public int currentHp { get; set; }
        public int maxHp { get; set; }
        public int currentMana { get; set; }
        public int maxMana { get; set; }
        public List<Skill> competences = new List<Skill>();

        public Being(string name, int currentHp, int maxHp, int currentMana, int maxMana, List<Skill> competences)
        {
            this.name = name;
            this.currentHp = currentHp;
            this.maxHp = maxHp;
            this.currentMana = currentMana;
            this.maxMana = maxMana;
            this.competences = competences;
        }

        public void Attack(Skill skill, Being target)
        {
            Console.WriteLine();
            Console.WriteLine(this.name + " | Attacking " + target.name + " with "+ skill.name + ".");
            int oldHp = target.currentHp;
            target.currentHp -= skill.damages;
            this.currentMana -= skill.manaCost;
            Console.WriteLine(target.name.ToUpper() + " lost " + (oldHp - target.currentHp) + " health points and is now at "
                + target.currentHp + " health.");
            Console.WriteLine();
        }
    }
}
