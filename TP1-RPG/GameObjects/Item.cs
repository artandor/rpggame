﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1_RPG.GameObjects
{
    public class Item
    {
        public string name;
        public string description;
        public int cost;
        public Dictionary<string, int> effects;
        public int lootChance = 100;

        public Item(string name, string description, int cost, Dictionary<string, int> effects, int lootChance=100)
        {
            this.name = name;
            this.description = description;
            this.cost = cost;
            this.effects = effects;
            this.lootChance = lootChance;
        }

        public void Use(ref Player player)
        {
            if (effects.ContainsKey("currentHp"))
            {
                player.currentHp += effects["currentHp"];
            }
            if (effects.ContainsKey("currentMana"))
            {
                player.currentMana += effects["currentMana"];
            }
            if (effects.ContainsKey("tauxSucre"))
            {
                player.tauxSucre += effects["tauxSucre"];
            }
        }

        public void printEffects()
        {
            foreach (KeyValuePair<string, int> effect in effects)
            {
                Console.WriteLine(effect.Key + " : " + effect.Value);
            }
        }
    }


}
