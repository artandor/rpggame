﻿using System;
using System.Collections.Generic;

using TP1_RPG.GameObjects;

namespace TP1_RPG.scenarios
{
    public class Scenario1
    {
        static bool finished = false;

        static Item candy = new Item("Candy", "Un bonbon tout bon qui vous régènerera des points de vie. C'est très sucré.", 3,
            new Dictionary<string, int> { { "currentHp", 2 }, { "tauxSucre", 3 } }, 50);
        static Item sandwich = new Item("Sandwich", "Some bread with ham and butter.", 3,
            new Dictionary<string, int> { { "currentHp", 2 }, { "tauxSucre", 1 } });
        static Item water = new Item("Bottle of water", "50cl of incredible water.", 2,
            new Dictionary<string, int> { { "currentHp", 1 } });
        static Item whisky = new Item("Bottle of whisky", "A little shot of ", 5,
            new Dictionary<string, int> { { "currentHp", 1 } });
        static Creature murloc = new Creature("Wild Murloc", 10, 10, 0, 2, new List<Skill> { new Skill("autohit", 2, 0, 1) }, 10, new List<Item>{candy}, 2, "Mglrmglmglmgl !!!!!!!!!!!!");
        static Creature guard = new Creature("Supermarket Guard", 8, 12, 1, 1, new List<Skill> { new Skill("autohit", 1, 0, 1), new Skill("insult", 3, 0, 1) }, 30, new List<Item> { sandwich }, 12);

        static public void Run(ref Player player)
        {
            Console.WriteLine("A wonderfull world appears in front of you ... And it's time for you to make your first choice ...");
            Console.WriteLine();
            Begin:
            Console.WriteLine("On your left, there is a supermarket, where you could find some very nice goods. On your right there is" +
                "a beautiful trail, bordered with some white birch.");
            Console.WriteLine();
            Console.Write("Where do you want to go ? (Right/Left) ");
            if (finished)
            {
                Console.Write("You can also go to next Scenario. (Continue) ");
            }
            string entry = "";
            bool validEntry = false;
            while (!validEntry)
            {
                entry = Console.ReadLine();

                switch (entry)
                {
                    case "Right":
                        Console.WriteLine("You start to walk on the trail. After a few meters, you see a shiny green point in a bush " +
                            "close to you.");
                        Console.WriteLine("Because you are a very strong hero, and not because i have more to do than writting a scenario, " +
                            "you walk to the green thing ...");
                        Console.WriteLine("OHHHHH It's not a 'thing' ........ IT'S A MURLOOOOOOCCCCCCCCCCC");
                        Console.WriteLine();
                        Console.Write("What do you want to do ? (Run/Fight)");
                        validEntry = true;
                        break;
                    case "Left":
                        Console.WriteLine("You enter that supermarket. There is a guard at the entry, he looks bored. You are a bit hungry, " +
                            "so you'll try to find some food. What do you want to get ?");
                        Console.Write("Candy/Sandwich/Water/Whisky");
                        validEntry = true;
                        break;
                    case "Continue":
                        if (!finished)
                        {
                            Console.WriteLine("You did not finish this scenario.");
                        }
                        else
                        {
                            goto End;
                        }
                        break;
                    default:
                        Console.WriteLine("Wrong Entry, try again.");
                        break;
                }
            }

            //Choose what to do in the forest or in the supermarket
            validEntry = false;
            while (!validEntry)
            {
                if(entry != "GetOutSuperMarket")
                {
                    entry = Console.ReadLine();
                }
                switch (entry)
                {
                    //Right
                    case "Run":
                        Console.WriteLine("You chose to run, before the murloc sees you. But damn ... It looks like he is now chasing you !");
                        Console.WriteLine("What do you want to do ? If you turn back and fight, the murloc might be surprised" +
                            " and be easier to beat ! (Fight/RunAgain)");
                        break;
                    case "RunAgain":
                        Console.WriteLine("Wooops ... It looks like you underestimated our new pal. The murloc is catching up to you." +
    "He gets on your back and start beating you. You are not able to get rid of him ...");
                        Console.WriteLine();
                        Console.WriteLine("He finally leaves you .. But you lost half of your Health.");
                        player.currentHp = player.currentHp / 2;
                        Console.WriteLine("In your despair, you see something on the ground.");
                        Console.WriteLine();
                        Console.WriteLine("Oh ! It's a " + candy.name + " ! You put it in your inventory.");
                        player.inventory.Add(candy);
                        validEntry = true;
                        break;
                    case "Fight":
                        FightManager.Fight(player, murloc);
                        player.Loot(murloc);
                        validEntry = true;
                        break;
                    //Left
                    case "Candy":
                        Console.WriteLine("You take a " + candy.name + " and put it in your bag. His effects are : ");
                        candy.printEffects();
                        Console.WriteLine();
                        entry = "GetOutSuperMarket";
                        break;
                    case "Sandwich":
                        Console.WriteLine("You take a " + sandwich.name + " and put it in your bag. His effects are : ");
                        sandwich.printEffects();
                        Console.WriteLine();
                        entry = "GetOutSuperMarket";
                        break;
                    case "Water":
                        Console.WriteLine("You take a " + water.name + " and put it in your bag. His effects are : ");
                        water.printEffects();
                        Console.WriteLine();
                        entry = "GetOutSuperMarket";
                        break;
                    case "Whisky":
                        Console.WriteLine("You take a " + whisky.name + " and put it in your bag. His effects are : ");
                        whisky.printEffects();
                        Console.WriteLine();
                        entry = "GetOutSuperMarket";
                        break;
                    case "GetOutSuperMarket":

                        Console.WriteLine("Perfect ! You now want to get out of the supermarket and head to the entry.");
                        Console.WriteLine("You check your money ... You have " + player.money + " golds in your inventory.");
                        Console.WriteLine("It's not enough to pay what you have in your inventory.");
                        Console.WriteLine();

                        Console.Write("What do you want to do ? Try to run at the door without paying ? " +
                            "Beg the cashier to let you go ? Give back all your items ? (Run/Beg/Give) ");

                        // Get out of the supermarket
                        int begCount = 0;
                        validEntry = false;
                        while (!validEntry)
                        {
                            entry = Console.ReadLine();
                            switch (entry)
                            {
                                case "Run":
                                    Console.WriteLine("You jump above the cashier like Renaud Lavillenie, run like Bolt to the door ..." +
                                        "But .. There is this bored guard ....");
                                    Console.WriteLine();
                                    Console.WriteLine("It's time for the DDDDDDDDDUEL !!!!");
                                    FightManager.Fight(player, guard);
                                    player.Loot(guard);
                                    validEntry = true;
                                    break;
                                case "Beg":
                                    if (++begCount >= 3)
                                    {
                                        Console.WriteLine("The cashier end up thinking you are a really poor person and let you go with what you took ..." +
                                            "(Congratz for finding an awesome trick here, that will learn your childs that harassement is a good thing)");
                                        Console.WriteLine();
                                        validEntry = true;
                                    } else
                                    {
                                        Console.WriteLine("The cashier tells you to go fuck yourself. It's a bit rude but come one, did you expect to not pay ?");
                                    }
                                    break;
                                case "Give":
                                    Console.WriteLine("You are not a fun hero, but you get out of the supermarket without a scratch." +
                                        "WAS IT REALLY WORTH ????? God what a waste of time. Anyway, your inventory is now empty.");
                                    player.inventory.Clear();
                                    validEntry = true;
                                    break;
                                default:
                                    Console.WriteLine("Wrong Entry, try again.");
                                    break;
                            }
                        }
                        break;
                    default:
                        Console.WriteLine("Wrong Entry, try again.");
                        break;
                }
            }

 

            finished = true;
            validEntry = false;
            Console.WriteLine("Congrats, you finished scenario 1, you can now go to Scenario 2 or farm more here." +
                "Care, you'll still lose health, and you might need it later.");
            goto Begin;
            End:
            Console.WriteLine("Congrats ! Going to Scenario 2 !!");
        }
    }
}
